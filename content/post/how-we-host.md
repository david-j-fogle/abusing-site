---
title: "How We Host"
date: 2018-04-25T11:19:30-05:00
draft: false
---

Welcome to the new abusing.technology front-page! Today, I want to give you some insight into how we host our website. I will detail the software used to generate the site, the docker configurations, and how the infrastructure itself is built. This will involve a short dive into Dockerfiles, kubernetes, rancher, tectonic, HAProxy, and Let's Encrypt's new wildcard certificates. We won't cover anything in depth, instead focusing on the glue between the tech will give you a more clear picture of how to use these technologies in your life.

The goodies are after the jump!

<!--more-->

## What even are these things? (What we glued together.)


Our infrastructure has become what I call "The Hipster's List of Infrastructure Technologies." You could go to Hacker News, find the most used words, I bet you the list matches almost one-to-one.

Joking aside, the setup is as follows. We use 2 kubernetes clusters, which reside on the same LAN, and share a loadbalancer. The LB uses LE certs, and provides an HTTP and HTTPS frontend, as well as a few TCP services on the network. HTTP services are simple to configure, you add a backend pointing to the kubernetes worker pools, add an ACL to the front-end, and reload HAProxy. This keeps everything loadbalanced, with stick-tables, and does full layer-7 service healthchecking, so I can avoid having to spend too much time monitoring things.  

The virtualization hosts are connected directly to the core network switch, as I like to keep the cable count between machines as close to zero as possible. Using virtualized nodes and masters allows easy backup, and snapshots taken before an untested change can save time in recovery if things break. (News Flash: Things are going to break.) I won't go into detail on how the hosts are set up, as it doesn't really matter outside of these hosts and their workload.

##### Kubernetes --- The guy driving the boat

Our k8s clusters are built in two very different ways, but function mostly the same way. The first cluster, with bigger nodes, was built using CoreOS's Tectonic Kuberntes distro. Persistant volumes are provided via Rook.io, allowing PVC's to be requested on the fly, and the data is replicated to all the nodes without further effort, allowing you to move your containers around the cluster very quickly, even if they have persistant data. The second cluster is a set of Ubuntu virtual machines, with Rancher 2.0 as the k8s distro. Rancher 2.0 allows you to provision a cluster in numerous ways, be it cloud providers, VMWare, OpenStack, etc, or in our case, a custom host running the rancher agent in docker, which provisions the k8s resources on the node to utilize it as a worker. In the end, both clusters work pretty much the same, the differences mainly being in the Tectonic-specific setup, the Ingress controller in particular. Rancher leaves you with a mostly-vanilla Kubernetes install. 

Kubernetes allows us to forget about the infrastructure, and focus on what we need for a service to function. The majority of our deployments are stateless containers, and the focus is entirely on having replicas available, allowing us to have mostly single YAML files for each service. Again, this lowers the amount of man-power required to keep the network healthy, as a full rebuild of the cluster can be accomplished in a few short kubectl commands to reprovision identical services. The current Tectonic cluster went from baremetal to serving web content in ~25 minutes, and has been up since without issue. The Rancher cluster has been in heavy flux, however, as it has been a testing platform for the Tech Previews of Rancher 2.0. 

## 
